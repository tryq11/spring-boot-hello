package com.kf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/templates")
public class TemplatesController {

	@RequestMapping("/hello")
	public String tmplates(){
		
		return "hello";
	}
	
	@RequestMapping("/helloFtl")
	public  String helloFtl(){
		return "helloFtl";
	}
}
