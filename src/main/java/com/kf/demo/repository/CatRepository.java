package com.kf.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.kf.demo.bean.Cat;

public interface CatRepository extends CrudRepository<Cat, Integer>{

	
}
