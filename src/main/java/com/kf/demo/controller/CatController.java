package com.kf.demo.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kf.demo.bean.Cat;
import com.kf.demo.service.CatService;


@RequestMapping("/cat")
@RestController
public class CatController {

	@Resource
	private CatService catService;
	
	@RequestMapping("/save")
	public void save(){
		Cat cat = new Cat();
		cat.setCatName("奥迪");
		cat.setAge(1);
		catService.save(cat);
		System.out.println("11111111111");
		System.out.println("2222222222222");
		System.out.println("666666666666");
	}
	
	@RequestMapping("/delete")
	public String delete(){
		catService.delete(1);
		return "delete ok";
	}
	
	@RequestMapping("/getAll")
	public Iterable<Cat> getAll(){
		return catService.getAll();
	}
	
	@RequestMapping("/selectByCatName")
	public Cat selectByCatName(String catName){
		return catService.selectByCatName(catName);
	}
	
}

