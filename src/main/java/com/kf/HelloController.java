package com.kf;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/hello")
	public String hello(){
		return "hello-万成";
	}
	
	@RequestMapping("/hello2")
	public String hello2(){
		return "hello-11111222222222333333344";
	}
	
	@RequestMapping(value = "/Dome", produces = "application/json; charset=utf-8")
	public Dome getDome(){
		Dome dome = new Dome();
		dome.setId(1);
		dome.setName("万成");
		dome.setCreateTime(new Date());
		dome.setRemarks("");
		return dome;
		
	}
}
