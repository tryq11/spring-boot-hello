package com.kf;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello2Controller {

	@RequestMapping("/hello3")
	public String hello3() {
		return "hello3";
	}
	
}
